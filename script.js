/* init */
var plr=true;           // true:p1(b),false:p2(w)
var ovr=false;          // game over status
var grid=new Array();   // the grid
var rows=7;             // rows
var cols=14;            // cols
var dbug=false;         // debug mode
var nln='\n';           // line break
var markerWhite='O';    // board marker
var markerBlack='X';    // board marker
var markerValid='+';    // board marker
var markerEmpty=' ';    // board marker
var cmp=true;           // computer opponent?
var lvl=1;              // cleverness (0:random,1:defensive,2:aggressive)
var dng=true;           // allow diagonal calculations
var stwin=4;            // how many stoines to win
var btnaud='';          // button audio
var clkaud='';          // color button audio
var erraud='';          // error audio



/* add class to element */
function addClassToElementById(elmid,elmcls){
  var cls=getClassStringForElementById(elmid);
  if(cls.includes(elmcls)==false){
    var newcls=cls.concat(' '+elmcls);
    document.getElementById(elmid).className=newcls;
  }
}

/* remove class from element */
function removeClassFromElementById(elmid,elmcls){
  var cls=getClassStringForElementById(elmid);
  var newcls=cls.split(elmcls).join('');
  document.getElementById(elmid).className=newcls;
}

/* generate box id for click */
function getBoxIdForRowAndCol(r,c){
  return 'box-'+r+'-'+c;
}

/* get the string with class names for element */
function getClassStringForElementById(elmid){
  return document.getElementById(elmid).className;
}

/* do click on element by id */
function clickOnElementById(elmid){
  document.getElementById(elmid).click();
}

/* hide cover layer */
function hideCover(){
  addClassToElementById('cover1','cover1hide');
  addClassToElementById('cover2','cover2hide');
}

/* center game */
function centerWrapper(){
  var left=Math.floor((document.documentElement.clientWidth-document.getElementById('wrapper').offsetWidth)/2);
  document.getElementById('wrapper').style.marginLeft=left+'px';
  var top=Math.floor((document.documentElement.clientHeight-document.getElementById('wrapper').offsetHeight)/2);
  document.getElementById('wrapper').style.marginTop=top+'px';
  var btn=document.getElementById('new').offsetWidth;
  var left=Math.floor((document.getElementById('wrapper').offsetWidth-(btn*3))/2)-25;
  document.getElementById('info').style.marginLeft=left+'px';
}

/* get start and end position in string for regex */
function getPositionForRegEx(str,regex,dir='>') {
  // setup
  dir=dir.toLowerCase();
  var pos=-1;
  var rex=new RegExp(regex);
  var match=rex.exec(str);
  // do we have a match and wht to do with it
  if(match!==null){
    switch(dir){
      case 'x':
        pos=match.index+1;
        break;
      case '<':
        pos=(match.index+match[0].length)-1;
        break;
      case '>':
      default:
        pos=match.index;
        break;
    }
  }
  // return
  return pos;
}

/* get length of trimmed string */
function getStringLength(str){
  var len=parseInt(str.trim().length);
  return len;
}

/* switch boxid to slotid */
function switchBoxidToSlotId(boxid){
  var boxid=boxid.replace('box','slot');
  return boxid;
}

/* minimum stones to block or go more aggressive */
function getThresholdForLevel(){
  if(lvl==1){return parseInt(stwin-2);}
  return parseInt(stwin-1);
}

/* search for regex in grid */
function getPosForRegExSearchInGrid(regex,dir='>'){
  var pos=-1;
  // horizontal
  for(r=0;r<rows;r++){
    var line='';for(c=0;c<cols;c++){line+=grid[r][c];}
    pos=getPositionForRegEx(line,regex,dir);
    if(pos>=0){break;}
  }
  // vertical
  if(pos==-1){
    for(c=0;c<cols;c++){
      var line='';for(r=0;r<rows;r++){line+=grid[r][c];}
      pos=getPositionForRegEx(line,regex,dir);
      if(pos>=0){pos=c;break;}
    }
  }
  // diagonal left-right-\
  if(dng==true&&pos==-1){
    var pr=0;
    var pc=cols-1;
    while(pr<rows){
      var p=0;var line='';while(parseInt(pc+p)<cols&&parseInt(pr+p)<rows){line+=grid[parseInt(pr+p)][parseInt(pc+p)];p++;}
      pos=getPositionForRegEx(line,regex,dir);
      if(pos>=0){pos+=pc;break;}
      // count
      if(pc>-1){pc--;}
      if(pc==-1){pc=0;pr++;}
    }
  }
  // diagonal top-down-/
  if(dng==true&&pos==-1){
    var pr=0;
    var pc=0;
    while(pc<cols){
      var p=0;var line='';while(parseInt(pc+p)<cols&&parseInt(pr-p)>=0){line+=grid[parseInt(pr-p)][parseInt(pc+p)];p++;}
      pos=getPositionForRegEx(line,regex,dir);
      if(pos>=0){pos+=pc;break;}
      // count
      if(pr<rows){pr++;}
      if(pr==rows){pr=rows-1;pc++;}
    }
  }
  // return
  return pos;
}

/* get amount of possible moves */
function getPossibleMovesAmount(){
  var amt=document.querySelectorAll('div.slot[attr-active="1"]').length;
  return amt;
}



/* generate dynamic game grid */
function generateGrid(){
  console.log('generate grid');
  for(r=0;r<rows;r++){
    grid[r]=new Array();
    for(c=0;c<cols;c++){
      grid[r][c]=markerEmpty;
    }
  }
}

/* refresh grid */
function refreshGrid(){
  console.log('refresh grid');
  setGridHtml(generateGridHtml());
  generateHints();
}

/* is grid empty */
function isGridEmpty(){
  var amt=document.querySelectorAll('div.box.pl1,div.box.pl2').length;
  if(amt==0){return true;}
  return false;
}

/* generate grid html */
function generateGridHtml(){
  console.log('generate grid html');
  var html='';
  // insert slots
  var arrow='&#x2B07';
  for(c=0;c<cols;c++){html+='        <div class="box slot" id="slot-0-'+c+'" attr-col="'+c+'" attr-win="0" attr-active="1">'+arrow+'</div>'+nln;}
  html+='        <div class="clear"></div>'+nln;
  // grid
  for(r=0;r<rows;r++){
    for(c=0;c<cols;c++){
      // show marker in debug mode
      var markstr='';
      if(dbug==true){markstr=grid[r][c];}
      // show colors for stones
      var colcls='';
      if(grid[r][c]==markerBlack){colcls+=' '+getColorNameForPlayer(true);}
      if(grid[r][c]==markerWhite){colcls+=' '+getColorNameForPlayer(false);}
      var boxid=getBoxIdForRowAndCol(r,c);
      html+='        <div class="box'+colcls+'" id="'+boxid+'">'+markstr+'</div>'+nln;
    }
    html+='        <div class="clear"></div>'+nln;
  }
  // return
  return html;
}

/* set grid html */
function setGridHtml(html){
  console.log('set grid html');
  document.getElementById('boxes').innerHTML=html;
}

/* set hints for possible moves */
function generateHints(){
  console.log('generate hints');
  // setup
  removeHints();
  myColorMarker=getMarkerForPlayer(plr);
  var regExLR='['+markerEmpty+']{1}['+myColorMarker+']{'+getThresholdForLevel()+'}';
  var regExRL='['+myColorMarker+']{'+getThresholdForLevel()+'}['+markerEmpty+']{1}';
  // mark win slot
  if(isGridEmpty()==false){
    var pos=getPosForRegExSearchInGrid(regExLR,'>');
    if(pos>=0&&pos<cols){
      var boxid=switchBoxidToSlotId(getBoxIdForRowAndCol(0,pos));
      document.getElementById(boxid).setAttribute('attr-win',1);
      if(dbug==true){addClassToElementById(boxid,'hint');}
    }
    var pos=getPosForRegExSearchInGrid(regExRL,'<');
    if(pos>=0&&pos<cols){
      var boxid=switchBoxidToSlotId(getBoxIdForRowAndCol(0,pos));
      document.getElementById(boxid).setAttribute('attr-win',1);
      if(dbug==true){addClassToElementById(boxid,'hint');}
    }
  }
  // set active slots
  console.log('set active slots');
  for(c=0;c<cols;c++){
    var line='';for(r=0;r<rows;r++){line+=grid[r][c];}
    var lnlen=getStringLength(line);
    // dectivate if full
    if(lnlen==rows){
      var boxid=switchBoxidToSlotId(getBoxIdForRowAndCol(0,c));
      document.getElementById(boxid).setAttribute('attr-active',0);
      document.getElementById(boxid).setAttribute('attr-win',0);
      addClassToElementById(boxid,'slotoff');
    }
  }
}

/* remove all hints */
function removeHints(){
  var elms=document.querySelectorAll('div.slot');
  elms.forEach((item)=>{
    var boxid=item.getAttribute('id');
    document.getElementById(boxid).setAttribute('attr-win',0);
    removeClassFromElementById(boxid,'hint');
  });
}

/* show hints for next move */
function toggleHints(){
  console.log('toggle hints');
  var elms=document.querySelectorAll('div.slot[attr-win="1"]');
  elms.forEach((item)=>{
    var boxid=item.getAttribute('id');
    if(getClassStringForElementById(boxid).includes('hint')==true){
      removeClassFromElementById(boxid,'hint');
    }else{
      addClassToElementById(boxid,'hint');
    }
  });
}



/* next player */
function nextPlayer(){
  togglePlayer();
  refreshGrid();
}

/* toggle player */
function togglePlayer(){
  removeClassFromElementById('plr','pl1');
  removeClassFromElementById('plr','pl2');
  // switch and set color for player
  plr=!plr;
  addClassToElementById('plr',getColorNameForPlayer(plr));
}

/* get marker for player */
function getMarkerForPlayer(plrflg){
  if(plrflg){return markerBlack;}
  return markerWhite;
}

/* get color name for player */
function getColorNameForPlayer(plrflg){
  if(plrflg){return 'pl1';}
  return 'pl2';
}



/* check for insert */
function insertStone(c){
  console.log('insert stone');
  // int them
  var c=parseInt(c);
  // position in column to place stone
  var regExLR='['+markerEmpty+']{1}[^'+markerEmpty+']{1}';
  var line='';for(r=0;r<rows;r++){line+=grid[r][c];}
  var lnlen=getStringLength(line);
  var pos=rows-1;
  if(getPositionForRegEx(line,regExLR)>0){pos=getPositionForRegEx(line,regExLR);}
  if(lnlen==rows-1){pos=0;}
  // place stone
  grid[pos][c]=getMarkerForPlayer(plr);
}

/* check if player has won */
function checkForWin(){
  console.log('check for win');
  var regEx='['+getMarkerForPlayer(plr)+']{'+stwin+'}';
  if(getPosForRegExSearchInGrid(regEx)>=0){endGame();}
}

/* no player can insert a stone */
function checkForNoPossibleInsert(){
  console.log('check for no insert');
  // no possible moves for both players
  var plrend=0;
  var amt=0;
  for(p=0;p<2;p++){
    amt=getPossibleMovesAmount();
    if(amt==0){plrend++;}
    togglePlayer();
  }
  // nothing to click for both players?
  if(plrend==2){endGame();}
}

/* deactivate insert slots */
function deactivateSlots(){
  // deactivate all slots
  var boxid='';
  var elms=document.querySelectorAll('div.slot[attr-active="1"]');
  elms.forEach((item)=>{
    boxid=item.getAttribute('id');
    document.getElementById(boxid).setAttribute('attr-active',0);
    document.getElementById(boxid).setAttribute('attr-win',0);
  });
}

/* mark winning row */
function markWinningRow(){
  // mark winning row
  var regExLR='['+getMarkerForPlayer(plr)+']{'+stwin+'}';
  var pos=-1;
  // horizontal
  for(r=0;r<rows;r++){
    var line='';for(c=0;c<cols;c++){line+=grid[r][c];}
    pos=getPositionForRegEx(line,regExLR);
    if(pos>=0){
      for(x=0;x<stwin;x++){
        boxid=getBoxIdForRowAndCol(r,parseInt(pos+x));
        addClassToElementById(boxid,'win');
      }
      break;
    }
  }
  // vertical
  if(pos==-1){
    for(c=0;c<cols;c++){
      var line='';for(r=0;r<rows;r++){line+=grid[r][c];}
      pos=getPositionForRegEx(line,regExLR);
      if(pos>=0){
        for(x=0;x<stwin;x++){
          boxid=getBoxIdForRowAndCol(parseInt(pos+x),c);
          addClassToElementById(boxid,'win');
        }
        break;
      }
    }
  }
  // diagonal left-right-\
  if(dng==true&&pos==-1){
    var pr=0;
    var pc=cols-1;
    while(pr<rows){
      var p=0;var line='';while(parseInt(pc+p)<cols&&parseInt(pr+p)<rows){line+=grid[parseInt(pr+p)][parseInt(pc+p)];p++;}
      pos=getPositionForRegEx(line,regExLR);
      if(pos>=0){
        for(x=0;x<stwin;x++){
          boxid=getBoxIdForRowAndCol(parseInt(pr+pos+x),parseInt(pc+pos+x));
          addClassToElementById(boxid,'win');
        }
        break;
      }
      // count
      if(pc>-1){pc--;}
      if(pc==-1){pc=0;pr++;}
    }
  }
  // diagonal top-down-/
  if(dng==true&&pos==-1){
    var pr=0;
    var pc=0;
    while(pc<cols){
      var p=0;var line='';while(parseInt(pc+p)<cols&&parseInt(pr-p)>=0){line+=grid[parseInt(pr-p)][parseInt(pc+p)];p++;}
      pos=getPositionForRegEx(line,regExLR);
      if(pos>=0){
        for(x=0;x<stwin;x++){
          boxid=getBoxIdForRowAndCol(parseInt(pr-pos-x),parseInt(pc+pos+x));
          addClassToElementById(boxid,'win');
        }
        break;
      }
      // count
      if(pr<rows){pr++;}
      if(pr==rows){pr=rows-1;pc++;}
    }
  }
}



/* computer player is making a move */
function computerMove(){
  console.log('compuer move');
  // setuo
  var pos=-1;
  // aggressive (2), defensive (1), random (0)
  switch(lvl){
    case 2:
      pos=computerMoveAggressive();
      break;
    case 1:
      pos=computerMoveDefensive();
      break;
    case 0:
    default:
      pos=computerMoveRandom();
  }
  // nothing found, do a random one
  if(pos<0){pos=computerMoveRandom();}
  // do click
  var boxid=switchBoxidToSlotId(getBoxIdForRowAndCol(0,pos));
  setTimeout(()=>{document.getElementById(boxid).click();},500);
}

/* defensive or aggressive differs just from the player flag */
function getBoxIdForDefensiveAndAggressiveMove(plrflag){
  // setup
  var pos=-1;
  // X_X
  var regExLR='['+getMarkerForPlayer(plrflag)+']{1}['+markerEmpty+']{1}['+getMarkerForPlayer(plrflag)+']{1}';
  pos=getPosForRegExSearchInGrid(regExLR,'x');
  // XX_ / _XX
  if(pos<0){
    var regExLR='['+markerEmpty+']{1}['+getMarkerForPlayer(plrflag)+']{'+getThresholdForLevel()+'}';
    var regExRL='['+getMarkerForPlayer(plrflag)+']{'+getThresholdForLevel()+'}['+markerEmpty+']{1}';
    pos=getPosForRegExSearchInGrid(regExLR,'>');
    if(pos<0){pos=getPosForRegExSearchInGrid(regExRL,'<');}
  }
  // return
  return pos;
}

/* do a more aggressive move */
function computerMoveAggressive(){
  return getBoxIdForDefensiveAndAggressiveMove(plr);
}

/* do defensive move */
function computerMoveDefensive(){
  return getBoxIdForDefensiveAndAggressiveMove(!plr);
}

/* do random move */
function computerMoveRandom(){
  // get possible moves
  var mvs=new Array();
  var elms=document.querySelectorAll('div.slot[attr-active="1"]');
  elms.forEach((item)=>{mvs.push('0,'+item.getAttribute('attr-col'));});
  // select a random one
  var which=Math.floor(Math.random()*mvs.length);
  var crd=mvs[which].split(',');
  return crd[1];
}



/* setup game audio files */
function setupAudio(){
  btnaud=new Audio('button.wav');
  btnaud.volume=0.15;
  clkaud=new Audio('click.wav');
  clkaud.volume=0.5;
  erraud=new Audio('error.wav');
  erraud.volume=0.5;
}



/* init game */
function init(){
  console.log('init');
  console.log('debug: '+dbug);
  console.log('cmp: '+cmp);
  console.log('lvl: '+lvl);
  // fresh start
  setupAudio();
  grid=new Array();
  generateGrid();
  refreshGrid();
  plr=false;
  nextPlayer();
  ovr=false;
  // and go
  console.log('ready!');
}

/* end of game */
function endGame(){
  console.log('game ended');
  erraud.play();
  ovr=true;
  deactivateSlots();
  markWinningRow();
}



/* listener for key */
document.addEventListener('keydown',(event)=>{
  switch(event.which){
    // n(ew)
    case 78:
      if(ovr==true){clickOnElementById('new');}
      break;
    // h(epl)
    case 72:
      if(ovr==false){clickOnElementById('hnt');}
      break;
    // r(eset)
    case 82:
      init();
      break;
  }
},false);

/* listener for click */
document.addEventListener('click',(event)=>{
  // clicked on a button
  if(event.target.matches('.button')){
    btnaud.cloneNode(true).play();
    var action=event.target.getAttribute('attr-action');
    console.log(action);
    // what to do?
    if(ovr==true&&action=='new'){init();}
    if(ovr==false&&action=='hnt'){dbug=!dbug;refreshGrid();}
  }
  // clicked on a box
	if(ovr==false&&event.target.matches('.slot')){
    clkaud.cloneNode(true).play();
    var val=parseInt(event.target.getAttribute('attr-active'));
    if(val==1){
      var col=parseInt(event.target.getAttribute('attr-col'));
      insertStone(col);
      refreshGrid();
      // check if player has won
      checkForWin();
      // check for no possible insert
      if(ovr==false){checkForNoPossibleInsert();}
      // toggle player
      if(ovr==false){nextPlayer();}
      // white (false) can be a computer player
      if(ovr==false&&cmp==true&&plr==false){computerMove();}
    }
  }
},false);



/* wait until document is fully loaded and ready to start */
let stateCheck=setInterval(()=>{
  if(document.readyState=='complete'){
    clearInterval(stateCheck);
    init();
    centerWrapper();
    hideCover();
    setTimeout(()=>{addClassToElementById('wrapper','rotatein');},2500);
  }
},250);
